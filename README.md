# Stooge - A Polaris Leap Automation

Stooge helps you waive specific kinds of fines and fees from multiple patron accounts. Written in AutoHotkey, Stooge utilises Polaris Leap, the browser based staff client for [Polaris ILS](https://www.iii.com/products/polaris-ils/) and Google Chrome. 

### Suggested Uses

Stooge is bot that simply rolls through a list of patron barcodes (kept in a CSV file), bringing up each account in Polaris Leap, waives a given type of fine, adds a note as it does so, and then closes the account before proceeding to the next one. Suggested uses include:

* Waiving existing overdue fines because your library is going fine free.
* Waiving only a certain kind of fine or fee in conjunction with a "Food for Fines" event.
* Clearing out old fines because your library no longer charges fees for that issue. 

### Instructions - Setting up

Stooge assumes that you're using Google Chrome to work with Polaris Leap and that you're logging into Leap using a staff account with the permissions necessary to waive fines. 

I highly recommend installing the [Window Resizer](https://chrome.google.com/webstore/detail/window-resizer/kkelicaakdanhinjdeammmilcgefonfh) extension and setting your window size to 1440 x 900. This gives Stooge plenty of room to work, and it provides a standard size. You can run Stooge on multiple machines or multiple virtual machines simultaneously, and setting a common browser window size avoids problems with the verifications Stooge uses to determine where it is in the process of waiving fines.

Each step of the way, Stooge is verifying where it is using the colours and locations of pixels. For example, it checks the colour of the Pay button to see if it's active, and that lets Stooge know that the fines have been selected and are ready to waive. Stooge has no interest in the Pay button, it's just a good way to check to make sure the fines have been winnowed down and selected before moving on.

You will need to change some of the colours and locations of the pixels Stooge looks for to determine where it is in its process and whether or not it can move forward. To do this, you can use AutoHotkey's Window Spy to gather a coordinate location within the Chrome window. Look at the Mouse Position listed in Window Spy and use the coordinates listed for Client.

There are times that Stooge looks to see whether or not a button is active and it does this by pixel colour. Remember, if your mouse hovers over a button in Leap, it changes the colour slightly. If you need to get a specific colour for a given location, free of a mouse hover, get the coordinates from Window Spy and feed those to the PixelColourTest.ahk script. When you run PixelColourTest, it gives you five seconds to make your Chrome window the active window and then provides your pixel colour in a message box.

If possible, test Stooge on a training server using dead/outdated data. Adjust the Sleep times as needed, though Stooge should run well on fast or slow hardware.

Stooge is set up to waive overdue fines, but this can be easily modified in the script. It works by filtering down the kinds of fines on the Leap Patron Account screen. Just alter the code to look for the fines you want to waive.

### Instructions - Deploying

You will need a list of patron barcodes corresponding to accounts with the type of fine you're looking to waive. While Stooge can handle as many accounts as you like, you'll need to consider your library's Leap usage patterns. For instance, when your libraries are open, busy, and all of them are using Leap; you may start to see problems with Stooge as IIS starts to lag a bit.

Stooge works out of whatever directory its in. Place one of your batches of barcodes in the same directory as Stooge and make sure that batch is named PatronOverdues.csv. The CSV should be nothing more than a list of barcodes with a carriage return after each one. Have a look at the example PatronOverdue.csv in this repo.

Launch Chrome and log into Polaris Leap. I recommend you use Window Resizer to set your Chrome window size to 1440 x 900. Make sure your Workforms sidebar is pinned open. Stooge works with one account at a time, so you should never see more than one account in that workform list. Click the OmniSearch box to make sure a flashing cursor is present where you'd normally scan a patron barcode.

Launch Stooge. It will automatically find the Chrome window and go to work. Stooge includes a GUI window to help you track the following information:

* Total Lines - The number of accounts or barcodes in your CSV file.
* Lines Remaining - The number of accounts left to process.
* Current Record - The account Stooge is working on right now.
* Previous Record - The account Stooge just finished.
* Percentage Complete - How far along your batch is.

The GUI window offers a Stop button to terminate the process at any time. While I recommend you monitor each instance of Stooge that's currently running, I also recommend you occasionally go outside, get lunch, and take an occasional restroom break. If you terminate the batch, a message box pops up to let you know what the current and previous account was. Edit the CSV file as needed to remove accounts Stooge already handled.

If your cursor is already in the OmniSearch box, Stooge kicks off with the first account in the list. At this point there is one last thing you need to do, and Stooge gives you plenty of time to do it.

One part of Stooge's process requires a mouse click, and that's clicking the Waive Charge button in Leap. For the very first account in the list, Stooge gives you eight seconds to move your mouse cursor over that button. **Do *not* click the button. Just put the mouse cursor over it.** Stooge clicks that button for you after eight seconds and, from there forward, it'll click it after a much shorter interval.

At that point, you can sit back and let Stooge do its thing. Stooge runs fine on multiple machines, both physical and virtual. At one point, I had Stooge running on eight different machines, two of them virtual. Just mind your batches and don't feed the same batches to two different Stooges. I recommend giving each Stooge a name; something creative like Moe, Larry, Curly, or Shemp. That helps keep things differentiated.

### Instuctions - Between batches

After Stooge processes your CSV file, I recommend logging out of Leap, clearing Chrome's cookies, data, and cache, and then closing Chrome out completely. If you're processing multiple batches, it's good to do this before setting up your next batch. That way Chrome is closed out and Windows frees up that memory. When you're ready to start your next batch, re-launch Chrome and continue on as described above. 

### Known Issues

Stooge occasionally falls over, usually due to lagging from IIS and, thus, Polaris Leap. However:

* Sometimes Stooge hangs up after opening a patron record, but before transitioning to the patron's account screen. Stooge won't do anything further until the patron account screen is open and you can click the Account link, and then move the mouse back to the Waive Charge button. Most of the time, Stooge will continue processing the batch.
* Similar to the above, Stooge will accidentally pop the word Account into the barcode number of a patron registration if that registration comes up as part of the patron lookup. (This happens in Leap when the account has expired.) Stooge will *not* alter the record nor does it have the ability to save it. After it does this, it hangs there waiting to move on. At this point I recommend terminating the batch, editing the CSV to take out the previously handled records, and relaunching Stooge. 
* If Stooge is behaving strangely, continuously hanging up and failing to move on properly, I recommend terminating the batch. Edit your CSV to remove the previously handled accounts. Log out of Leap. Then clear the cookies, cache, and data in Chrome and close Chrome out for ten seconds or so to free up memory. I've noticed that Chrome gets bogged down with processing hundreds of accounts in Leap. Clearing Chrome's cache, closing it, relaunching, and then restarting your batch from a fresh browser seems to cure a bunch of problems.

### Deployments

**Maricopa County Library District, Arizona** - Summer 2019: Overdue fines abolished, all existing overdue fines waived.

**Moorpark City Library, California** - October 2023: Overdue fines abolished, all existing overdue fines waived.

**Osceola Library System, Florida** - January 2024:  All replacement card fees waived.

**Red Oak Public Library, Texas** - January 2024: Overdue fines abolished, all existing overdue fines waived.

### Questions?

Feel free to contact me: cyberpunklibrarian (at) protonmail (dot) com.
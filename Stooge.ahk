; STOOGE - An overdue fine waiving bot 
; Polaris Leap Automation
; Coded by Daniel Messer - cyberpunklibrarian@protonmail.com
; Testing 2019/04/24
; Deployed 2019/05/22 at Maricopa County Library District - Phoenix, AZ
; Deployed 2023/10/01 at Moorpark City Library - Moorpark, CA

#SingleInstance Force
SendMode, Input
SetWorkingDir %A_ScriptDir% ;Home is where the script is.
CoordMode, Pixel, Client
csvDataFile := "PatronOverdues.csv" ;Primary input file.

; Let's get some kinda idea how long this will take

Loop, Read, %csvDataFile%
{
   total_lines = %A_Index%
}

; Build the GUI monitoring.

lines_remain := total_lines

Gui, +AlwaysOnTop
Gui, Font, s14 bold, Arial
Gui, Add, Text,, Waives in progress
Gui, Font, s10 norm, Arial
Gui, Add, Text,, Total Lines: %total_lines%
Gui, Add, Text,vRem, Lines Remaining: %lines_remain%
Gui, Add, Text,vCurrent, Current record: xxxxxxxxxxxxxxx
Gui, Add, Text,vPrevious, Previous record: xxxxxxxxxxxxxxx
Gui, Add, Text,vPerc, Percentage Complete: 0.0000
Gui, Add, Button, gKillApp, &Stop ;Give you the ability to manually kill the script.
Gui, Show, x1 y1 h250 w310, Patron Waive Progress
Sleep, 2000

; This sets an initial click delay of 8 seconds to get your mouse cursor over the Waive Charge button
; This variable is altered after the first loop and the script comes up to speed
ClickDelay := 8000

; Parse the data in the file.
Loop, Read, %csvDataFile%

{
	Loop, Parse, A_LoopReadLine, CSV
 {
	; Assign variables to data.
	if (A_Index = 1)
		recordID := A_LoopField
	}
	
; Begin sending the data to Polaris Leap
; Make sure Leap is open, logged in, and the cursor is in the OmniSearch box.
	
	GuiControl,,Current, Current record %recordID%
	SetTitleMatchMode, 2
	WinActivate, Polaris Leap - Staff
	Send %recordID%
	Sleep, 500
	Send {ENTER}
	;Sleep, 2000
	
	SetTitleMatchMode RegEx
	WinWaitActive, Patron - .*
	
	
; ADJUST FOR PIXEL COLOUR AS NEEDED
; This loop checks to see if the generic headshot icon is on-screen	
	Loop 
	{
		PixelGetColor, headcheck, 315, 323
		if headcheck = 0xBCBCBC
			break
	}
	
	Sleep, 1000
	Send ^f
	Sleep, 500
	Send Account
	Sleep, 500	
	Send {ESC}
	Send {Enter}
	WinWaitActive, Patron - .* - fees
	Sleep, 1000

; ADJUST FOR PIXEL COLOUR AS NEEDED
; This loop checks to see if the disabled Pay button is on-screen
	
	Loop 
	{
		PixelGetColor, paycheck, 358, 429, RGB
		if paycheck = 0x7CA7B2
			break
	}
	
	Send {TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}
	Sleep, 500
	Send Overdue Item
	Sleep, 500
	Send {TAB}
	Send {SPACE}
	Sleep, 500

; ADJUST FOR PIXEL COLOUR AS NEEDED
; This loop checks to see if the Pay button is enabled. If so, we can click Waive Charge.
	
	Loop 
	{
		PixelGetColor, waivecheck, 358, 429, RGB
		if waivecheck = 0x367888
			break
	}
	
	Sleep, %ClickDelay%	
	MouseClick, Left
	ClickDelay := 500 ;After the initial 8 second click delay, set it to half a second going forward.
	Sleep, 1000
	Send {TAB}
	Send SCRIPTED MESSAGE - Moorpark fine free - cleared by Stooge. daniel.messer@lsslibraries.com
	Sleep, 500
	Send {TAB}
	Sleep, 500
	Send {ENTER}
	Sleep, 2000

; ADJUST FOR PIXEL COLOUR AS NEEED
; This loop checks to see if the Pay button is disabled. If so, the waives have completed.
	
	Loop 
	{
		PixelGetColor, donecheck, 358, 429, RGB
		if donecheck = 0x7CA7B2
			break
	}
	
	Send ^f
	Sleep, 500
	Send Close All
	Sleep, 500
	Send {ESC}
	Sleep, 1000
	Send {ENTER}
	Sleep, 500
	
; ADJUST FOR PIXEL COLOUR AS NEEDED
; This loop checks to see if the screen has cleared. If so, move on.
	
	Loop 
	{
		PixelGetColor, greycheck, 294, 405, RGB
		if greycheck = 0xE5E3E1
			break
	}
	
; Update GUI monitoring.
	
; Update the maths.
	lines_remain -= 1
	update_percent := Round(((total_lines - lines_remain) / total_lines) * 100)
	
; Update the GUI
	previous := recordID
	GuiControl,,Previous, Previous record: %previous%
	GuiControl,,Rem, Lines Remaining: %lines_remain%
	GuiControl,,Perc, Percentage Complete: %update_percent%

; Check to see if we're done.	
	If lines_remain = 0
	{
		MsgBox,0,Complete,Waives complete.`nAll accounts processed.
		ExitApp
	}

}
	
; Code for the Stop button.
KillApp:
{
	MsgBox,0,Script Terminated,Current record: %recordID%`nPrevious record: %previous%
	ExitApp
}